// Required C++14 Complier
// Tested with VS C++ 2017
// Problem: The power sum challenge from https://fizzbuzzer.com/power-sum-challenge/
/*
Text from: https://fizzbuzzer.com/power-sum-challenge/
Find the number of ways that a given integer, X, can be expressed as the sum of the N^{th} power of unique, natural numbers.

Input Format
The first line contains an integer X.
The second line contains an integer N.

Constraints
1 <= X <= 1000
2 <= N <= 10

Output Format
Output a single integer, the answer to the problem explained above.

Sample Input 0
10
2

Sample Output 0
0

Explanation 0
If X = 10 and N=2, we need to find the number of ways that 10 can be represented as the sum of squares of unique numbers.

10 = 1^2 + 3^2
This is the only way in which 10 can be expressed as the sum of unique squares.

Sample Input 1
100
2
Sample Output 1
3

Explanation 1
100 = 10^2 = 6^2 + 8^2 = 1^2 + 3^2 + 4^2 + 5^2 + 7^2

Sample Input 2
400
2

Sample Output 2
55
*/

#include <sstream>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <numeric>
#include <vector>
#include <cassert>


namespace PowerOfSum
{
  // Generate combination vector then calling a function.
  // @param n: maximum natural number to be generated
  // @param r: size of combination vector
  // @param fn: a function that will be called after the combination is generated
  template<typename FN>
  void generateCombinationThen(int n, int r, FN&& fn)
  {

    auto arrayMask = std::vector<bool>(n, true);
    std::fill(arrayMask.begin(), arrayMask.begin() + r, false);

    for (;;)
    {
      std::vector<int> com;
      com.reserve(r);
      int i = 1;
      for (auto b : arrayMask)
      {
        if (!b)
        {
          com.push_back(i);
        }
        i++;
      }
      fn(com);

      if (!std::next_permutation(arrayMask.begin(), arrayMask.end()))
        break;
    }
  }

  // Check if the accumulated vector with power equals to expected value.
  // @param vec: a combination vector
  // @param exp: exponent number
  // @param expectedNumber: a number to be compared
  // @return: result of accumulated combination vector with power
  bool isAccumlateWithPowerEqual(const std::vector<int>& vec, int exp, int expectedNumber)
  {
    int sum = 0;
    for (auto v : vec)
    {
      sum += static_cast<int>(pow(v, exp));
      if (sum > expectedNumber)
        return false; 
    }
    return sum == expectedNumber;
  }


  // Count expressing sum.
  // @param vec: a combination vector
  // @param exp: exponent number
  // @param expectedNumber: a number to be compared
  // @return: number of ways that a given integer can be express as the sum of the power of unique, natural numbers
  size_t countExpressingSum(int xInput, int nInput)
  {
    const auto root = pow(static_cast<double>(xInput), 1.0 / static_cast<double>(nInput));
    const auto maxPossibleNum = static_cast<int>(floor(root));

    size_t equalCount = 0;

    for (int i = 1; i <= maxPossibleNum; i++)
    {
      generateCombinationThen(maxPossibleNum, i, [&](const std::vector<int>& comVec)
      {
        if (isAccumlateWithPowerEqual(comVec, nInput, xInput))
        {
          equalCount++;
        }
      });
    }

    return equalCount;
  }

  // Unit test
  void runTests()
  {
    assert(countExpressingSum(10, 2) == 1);
    assert(countExpressingSum(100, 2) == 3);
    assert(countExpressingSum(100, 3) == 1);
  }
}

int main()
{
  using namespace std;
  using namespace PowerOfSum;

  //runTests();

  std::cout << "This is a program to find the number of ways that a given integer can be express as the sum of the power of unique, natural numbers." << std::endl;
  std::cout << "input the integer and pass enter." << std::endl;
  int XInput;
  std::cin >> XInput;

  if (XInput < 1 && XInput > 1000)
    return EXIT_FAILURE;

  std::cout << "input another integer and pass enter." << std::endl;

  int NInput;
  std::cin >> NInput;

  if (NInput < 2 && NInput > 10)
    return EXIT_FAILURE;

  std::cout << "Answer: " << countExpressingSum(XInput, NInput) << std::endl;
  return EXIT_SUCCESS;
}
