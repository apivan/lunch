#!/usr/bin/env python

"""TimeConverter.py: 24 hours clock to 12 hours clock string converter """

from datetime import *

def convertTime(inputStr) :
    """ Retrun a converted from 24 hours clock to 12 hours clock string.
        inputStr -- the input string
    """
    try:
        inputTime = datetime.strptime( inputStr, '%I:%M:%S%p')
        return inputTime.strftime('%H:%M:%S')
    except ValueError:
        print("Invalid input")
        return ""
    except TypeError:
        print("Internal error")
        return ""


def runTests():
    """ unit test """
    assert convertTime("07:05:45PM") == "19:05:45"
    assert convertTime("01:30:05PM") == "13:30:05"
    assert convertTime("12:00:00PM") == "12:00:00"
    assert convertTime("12:00:00AM") == "00:00:00"

#runTests()

userInputStr = str(input("Write the time in 12 hour format as 07:05:45PM\n"))
print(convertTime(userInputStr))


