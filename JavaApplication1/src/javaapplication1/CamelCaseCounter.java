/*
Required JDK 8u151. 
*/
package javaapplication1;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CamelCaseCounter {

    /**
    * Exception class for bad camel case.
    */
    public static class BadCamelCaseException extends Exception {

        public BadCamelCaseException(String message) {
            super(message);
        }
    }

    /**
    * Count the words in CamelCase.
    * @param inputString
    * @throws BadCamelCaseException
    * @return number of words in that input camel case.
    */
    public static long countCamelCaseWords(String inputString) throws BadCamelCaseException {
        inputString = inputString.trim();

        if (inputString.isEmpty()) {
            throw new BadCamelCaseException("String is empty.");
        }

        if (!Character.isLowerCase(inputString.charAt(0))) {
            throw new BadCamelCaseException("The first word is not start with lower case.");
        }

        if (inputString.chars().count() > 100000) {
            throw new BadCamelCaseException("String is too long.");
        }

        return inputString.chars().filter((c) -> Character.isUpperCase(c)).count() + 1;
    }

    /**
    * Run the built-in unit tests.
    */
    public static void runTests() {
        try {
            assert countCamelCaseWords("saveChangesInTheEditor") == 5;
            assert countCamelCaseWords("camelCase") == 2;
        } catch (BadCamelCaseException ex) {
            assert false;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Type the CamelCase string and pass enter.\n");

        String inputString = in.next();
        inputString = inputString.trim();

        //runTests();

        try {
            long count = countCamelCaseWords(inputString);
            System.out.println("Answer: " + count);
        } catch (BadCamelCaseException ex) {
            Logger.getLogger(CamelCaseCounter.class.getName()).log(Level.SEVERE, null, ex);
            System.out.print(ex.getMessage());
            System.exit(1);
        }
    }

}
